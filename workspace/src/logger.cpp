#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>

int  main(void){
int fd;
int puntos[3];

	if(mkfifo("FIFO",0600)<0){
        perror("No se crea FIFO");
		return 0;
    }
	if((fd=open("FIFO", O_RDONLY))<0){
		perror("No se abre FIFO");
			return 1;
		}
	while(read(fd, puntos,sizeof(int)*3 )==3*sizeof(int)){
	 if(puntos[2]==1){

            printf("Jugador 1 marca 1 punto, lleva un total de %d \n",puntos[0]);

        }else if(puntos[2]==2){

            printf("Jugador 2 marca 1 punto, lleva un total de %d \n",puntos[1]);
        }
	}
close(fd);
unlink("FIFO");
return 0;
}

