// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>

#include <sys/mman.h>

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <pthread.h>
#include <signal.h>
using namespace std; 
//int PID;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static void FINAL_SIGNAL(int s){
	printf("Señal capturada : %s \n",strsignal(s));
	if(s==31 || s== 12 || s==17)
		printf("Valor retornado: 0\n");
	else
		printf("Valor retornado: %d\n", s);
	//sleep(1);
	//kill(PID,SIGTERM);
	//sleep(1);
	exit(1);
}
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            if((read(fdCs, cad, sizeof(cad)))<0){
            	perror("ERROR : Lectura FIFO_ClienteServidor");
            	exit(1);
            }
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
    close(fd);      //Cerrando tuberia
    
    close(fdCs);	//Cerrando FIFO_ClienteServidor.
    
    close(fdSc);	//Cerrando FIFO_ServidorCliente.
}
void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int i=0; i<esferas.size();i++)
        esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{
    char cad[200];
    float distancia=100;
    int numero=0;
    int nbolas=0;

    ///////////////////////////////////////////////////////////////////////////////////
   //sigprocmask(SIG_BLOCK, &intmask,NULL); //////////////////////////////////////////// BLOQUEO RECEPCION DE SEÑALES 
    ///////////////////////////////////////////////////////////////////////////////////
    
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	for(int i=0; i<esferas.size();i++)
        esferas[i]->Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
        for(int j=0; j<esferas.size();j++)
            paredes[i].Rebota(*(esferas[j]));
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

    for(int i=0; i<esferas.size();i++){                         ///Cada vez que se detecta un rebote contra una raqueta se suma en el contador de rebotes de cada esfera
	if(jugador1.Rebota(*(esferas[i])))
        esferas[i]->contador+=1;
	if(jugador2.Rebota(*(esferas[i])))
        esferas[i]->contador+=1;
    }

    for(int i=0; i<esferas.size();i++){                         ///Si una esfera tiene mas de 5 rebotes su radio pasa a ser 2/3 del radio actual.
    if(esferas[i]->contador >=5){
        esferas[i]->radio=2*esferas[i]->radio/3;
        esferas[i]->contador=0;
        if(esferas.size()<3){                                   ///Si el numero de esferas es menor que tres se crea una esfera.
        esferas.push_back(new Esfera);
        }
        if(esferas[i]->radio<0.2f){                             ///Si alguna de las esferas tiene el radio menor que 0.2 se elimina.
            delete esferas[i];
            esferas.erase(esferas.begin()+i);
        }
	}
	}
    for(int i=0; i<esferas.size();i++){
	if(fondo_izq.Rebota(*(esferas[i])))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

        	puntos[0]=puntos1;      //Puntos jugador 1.
		puntos[1]=puntos2;      //Puntos jugador 2.
		puntos[2]=2;            //Jugador que marca punto.

		write(fd,puntos,sizeof(int)*3);
	}
	}

    for(int i=0; i<esferas.size();i++){
	if(fondo_dcho.Rebota(*(esferas[i])))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		puntos[0]=puntos1;      //Puntos jugador 1.
		puntos[1]=puntos2;      //Puntos jugador 2.
		puntos[2]=1;            //Jugador que marca punto.

		write(fd,puntos,sizeof(int)*3);
	}
    }
    if(puntos1==3){ ///Si algun jugador llega a 3 puntos activa la funcion Fin del juego.
    nbolas=1001;
        	if((write(fdSc, &nbolas, sizeof(int)))<0){
			perror("ERROR: Escritura FIFO_ClienteServidor(Numero Esferas).");
			exit(1);
		}
	sleep(1);                                
        FIN(true,1);
    }else if(puntos2==3){
    nbolas=1002;
    		if((write(fdSc, &nbolas, sizeof(int)))<0){
    			perror("ERROR: Escritura FIFO_ClienteServidor(Numero Esferas).");
			exit(1);
		}
	sleep(1);
        FIN(true,2);
    }else{     
    nbolas=esferas.size();
	    	if((write(fdSc, &nbolas, sizeof(int)))<0){
		perror("ERROR: Escritura FIFO_ClienteServidor(Numero Esferas).");
		exit(1);
		}
	}  
	for(int i=0; i<esferas.size();i++){
		if((write(fdSc, esferas[i], sizeof(Esfera)))<0){
			perror("ERROR: Escritura FIFO_ClienteServidor(EsferasVector).");
			exit(1);
		}
	}

sprintf(cad,"%f %f %f %f %f %f %f %f %d %d",jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	if((write(fdSc, cad, sizeof(char)*200))<0){
		perror("ERROR : Escritura FIFO_ClienteServidor(Puntuaciones & Jugadores)");
	}
		//sigprocmask(SIG_UNBLOCK, &intmask,NULL); // DESBLOQUEO RECEPCION DE SEÑALES 
}
void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void CMundoServidor::Init()
{
	Plano p;
	esferas.push_back(new Esfera);                                              ///Creacion esferas
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	fd=open("FIFO",O_WRONLY);
	if(fd<0)
        perror("ERROR APERTURA FIFO ESCRITURA");
        
        //Apertura FIFO_ClienteServidor.
        fdCs=open("FIFO_ClienteServidor",O_RDONLY);
        if(fdCs<0){
        perror("ERROR : Apertura Servidor FIFO_ClienteServidor");
        exit(1);
        }
        
        //Apertura FIFO_ServidorCliente.
        fdSc=open("FIFO_ServidorCliente",O_WRONLY);
        if(fdSc<0){
        	perror("ERROR: Apertura Servidor FIFO_ServidorCliente");
        	exit(1);
        }
        /*
        if((read(fdCs, &PID, sizeof(int)))<0){
        	perror("ERROR CLIENTE : Lectura PID FIFO_ClienteServidor");
            	exit(1);
    	}
    	printf("%d\n",PID);
        */
        pthread_create(&thid1, NULL, hilo_comandos, this);			//Declaracion hilo con la funcion a ejecutar.
        
        struct sigaction acc;
        acc.sa_handler=&FINAL_SIGNAL;
       // sigaddset(&acc.sa_mask,SIGPIPE);

        if(sigaction(SIGPIPE, &acc,NULL)<0){
        	perror("ERROR Servidor : Sigaction SIGPIPE");
        }
        
      	if(sigaction(SIGINT, &acc,NULL)<0){
        	perror("ERROR Servidor : Sigaction SIGINT");
        }
        
        if(sigaction(SIGTERM, &acc,NULL)<0){
        	perror("ERROR Servidor : Sigaction SIGTERM");
        }
        
      	if(sigaction(SIGUSR2, &acc,NULL)<0){
        	perror("ERROR Servidor : Sigaction SIGUSR2");
        }
    /* 								
    sigemptyset(&intmask);
    sigaddset(&intmask,SIGINT);
    sigaddset(&intmask,SIGTERM);
    sigaddset(&intmask,SIGUSR2);
    sigaddset(&intmask,SIGPIPE);
*/
}


void CMundoServidor::FIN(bool v,int p){
    if(v==true){
	printf("Servidor: Victoria, jugador %d ha llegado a 3 puntos\n" , p);
        exit(1);
    }
}
