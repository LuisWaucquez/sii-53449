// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>

#include <sys/mman.h>

#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <signal.h>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static void FINAL_SIGNAL(int s ){
    	//unlink("FIFO_ClienteServidor");
    	//unlink("FIFO_ServidorCliente");  
    	//unlink("DatosMemCompartida");
    	printf("Exit Cliente\n");
    	exit(1);  	
}
CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
    
    close(fdCs);
    unlink("FIFO_ClienteServidor");
    
    close(fdSc);
    unlink("FIFO_ServidorCliente");
    
    munmap(Datosp,sizeof(Datos));
    unlink("DatosMemCompartida");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int i=0; i<esferas.size();i++)
        esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{
    float distancia=100;
    int numero=0;
    int nbolas=0;
    char cad[200];
    ///////////////////////////////////////////////////////////////////////////////////
    sigprocmask(SIG_BLOCK, &intmask,NULL); //////////////////////////////////////////// BLOQUEO RECEPCION DE SEÑALES 
    ///////////////////////////////////////////////////////////////////////////////////
    
   if((read(fdSc, &nbolas,sizeof(int))<0)){
   	perror("ERROR : Lectura FIFO_ServidorCliente(Numero bolas)");
   	exit(1);
   }
   if(nbolas==1001){
   	FIN(true,1);
   }
   if(nbolas==1002){
   	FIN(true,2);
   }
   	
   
   if(esferas.size()>nbolas){
   	while(esferas.size()>nbolas){	//Pongo mi vector de esferas al mismo  tamaño que el del servidor.        //Si es mayor tamaño que servidor borro
        	esferas.pop_back();	
   	}
   }
   if(esferas.size()<nbolas){
   	while(esferas.size()<nbolas){										    //Si es menor tamaño que servidor sumo nueva esfera.
   	   	esferas.push_back(new Esfera);
   }	
   }
   for(int i=0; i<esferas.size();i++){
   	if((read(fdSc, esferas[i],sizeof(Esfera))<0)){
   	perror("ERROR : Lectura FIFO_ServidorCliente(Numero bolas)");
   	exit(1);
   }
   }
   
   	if((read(fdSc, cad,sizeof(char)*200)<0)){
   	perror("ERROR : Lectura FIFO_ServidorCliente(Puntuaciones & Jugadores)");
   	exit(1);
	}	
   sscanf(cad,"%f %f %f %f %f %f %f %f %d %d",&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1, &puntos2);
  
    for(int j=0; j<esferas.size();j++){                         ///Calcula la bola mas cercano en eje x a la raqueta 1 y lo guarda en variable numero
    float dist;
        dist=(esferas[j]->centro.x-esferas[j]->radio);
        if(dist<=0){
            dist= dist+abs(jugador1.x1);
            dist=abs(dist);
        }else{
            dist=(esferas[j]->centro.x+esferas[j]->radio)-jugador1.x1;
            dist=abs(dist);
        }
        if (dist<distancia && esferas[j]->velocidad.x<0){
            numero=j;
            distancia=dist;
        }
    }


    Datosp->raqueta1=jugador1;
    Datosp->esfera=*(esferas[numero]);
    if(Datosp->accion ==1){                                      ///En funcion del valor de accion sube o baja.
        OnKeyboardDown('w',0,0);
        }
    if(Datosp->accion ==-1){
        OnKeyboardDown('s',0,0);
        }
		sigprocmask(SIG_UNBLOCK, &intmask,NULL); // DESBLOQUEO RECEPCION DE SEÑALES        
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	  sigprocmask(SIG_BLOCK, &intmask,NULL); //////////////////////////////////////////// BLOQUEO RECEPCION DE SEÑALES 
	if((write(fdCs,&key,sizeof(unsigned char)))<0){
		perror("ERROR  : Escritura FIFO_ClienteServidor");
		exit(1);
	}
	sigprocmask(SIG_UNBLOCK, &intmask,NULL); // DESBLOQUEO RECEPCION DE SEÑALES 	
}

void CMundoCliente::Init()
{
	Plano p;
                                             
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

    Datos.accion=0;
    Datos.esfera;
    Datos.raqueta1=jugador1;
    fd1=open("DatosMemCompartida",O_RDWR |O_TRUNC | O_CREAT , 0666);
    	if(fd1<0)
        perror("ERROR APERTURA FICHEROMAP ");
    write(fd1,&Datos,sizeof(Datos));
    Datosp =static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(Datos),PROT_READ | PROT_WRITE ,MAP_SHARED,fd1,0));
    if(Datosp==MAP_FAILED){
        perror("ERROR Proyeccion en memoria");
    close(fd1);
    return;
    }
    close(fd1);
    
    /////////////////FIFO_ClienteServidor
    if(mkfifo("FIFO_ClienteServidor",0666)<0){
    	perror("ERROR : Crear FIFO_ClienteServidor");
    	exit(1);
    }
    
    fdCs=open("FIFO_ClienteServidor",O_WRONLY);
    if(fdCs<0){
    	perror("ERROR : Apertura Modo lectura FIFO_ClienteServidor");
    	exit(1);
    }
    
    if((mkfifo("FIFO_ServidorCliente",0666))<0){
    	perror("ERROR : Crear FIFO_ServidorCliente");
    	exit(1);
    }
    
    fdSc=open("FIFO_ServidorCliente",O_RDONLY);
    if(fdSc<0){
    	perror("ERROR : Apertura Modo Escritura FIFO_ServidorCliente");
    	exit(1);
    } 
    /*
    int PID;
    PID=getpid();
    if(write(fdCs,&PID,sizeof(int))<0){
    	perror("ERROR CLIENTE : Escritura PID");
    }
        struct sigaction acc;
        acc.sa_handler=&FINAL_SIGNAL;
        sigaddset(&acc.sa_mask,SIGPIPE);
        if(sigaction(SIGTERM, &acc,NULL)<0){
        	perror("ERROR CLIENTE : Sigaction SIGTERM");
        }
   
    sigemptyset(&intmask);
    sigaddset(&intmask,SIGTERM);      	
	*/
}

void CMundoCliente::FIN(bool v,int p){
    if(v==true){
        printf("Cliente: Victoria, jugador %d ha llegado a 3 puntos\n" , p);
        Datosp->esfera.centro.y= 400; 
 	exit(1);
    }
    
}
