// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void FIN(bool v , int p);
	
	void RecibeComandosJugador(); 

	std:: vector<Esfera *> esferas;                 //Vector de punteros a clase tipo Esfera para generar varias esferas.
	std:: vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fd;	//Indentificador FIFO
	int fd1;    //Indentidicador FicheroDatosCompartidos.
	int fdCs;   //Indentificador FIFO_ClienteServidor.
	int fdSc;   //Indentificador FIFO_ServidorCliente.
	int puntos[3];
    DatosMemCompartida *Datosp;
    DatosMemCompartida Datos;
    
    sigset_t intmask;////Bloquear señales en Ontimer  
    
    pthread_t thid1;	//Hilo
    
    
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
